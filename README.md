Adaptation des deux applications "Écrire en chiffres" et "Écrire en lettres" de la compilation d'applications clicmenu.

Écrire en chiffres des nombres proposés en lettres, ou inversement selon le type d'exercice choisi. Sont proposés 6 niveaux pour chaque type d'exercice.

L'application peut être testée en ligne [ici](https://primtux.fr/applications/chiffres-lettres/index.html)
